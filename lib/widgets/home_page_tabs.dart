import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_miruna/pages/upsert_page.dart';
import 'package:flutter_miruna/store/changenotifier.dart';
import 'package:flutter_miruna/store/models.dart';
import 'package:flutter_miruna/store/models/BaseModel.dart';
import 'package:flutter_miruna/store/ui.dart';
import 'package:flutter_miruna/widgets/vertical_expand.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../helpers.dart';

Map<TABS, Map> tabsConf = {
  TABS.CLIENTS: {
    'label': 'Pacienţi',
    'eventFilter': UseState("clientEventFilter", null, distinctValues: true),
    'selected': UseState("selectedClientID", null, distinctValues: true),
    'query': UseState("clientQuery", ''),
  },
  TABS.CONTACTS: {
    'label': 'Contacte',
    'eventFilter': UseState("contactEventFilter", null, distinctValues: true),
    'selected': UseState("selectedContactID", null, distinctValues: true),
    'query': UseState("contactQuery", ''),
  },
};
final _tabKeys = tabsConf.keys.toList();

// enum _eventFilters {
//   TODAY,
//   TOMORROW,
//   THIS_WEEK,
// }

Map<String, PageStorageKey> keys = {};
Key makeKey(String id) {
  if (keys[id] == null) {
    keys[id] = PageStorageKey(id);
  }
  return keys[id];
}

class HomePageTabs extends StatefulWidget {
  HomePageTabs({
    Key key,
  }) : super(key: key);

  @override
  _HomePageTabsState createState() => _HomePageTabsState();
}

class _HomePageTabsState extends State<HomePageTabs>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: _tabKeys.length, vsync: this);
    _tabController.addListener(() {
      debugPrint('tab changed ${_tabController.index}');
      UI().currentTab = _tabKeys.asMap()[_tabController.index];
    });
  }

  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  Widget buildClientTabHeader(BuildContext context) {
    //ignore: unused_local_variable
    final subscribe = Provider.of<UI>(context);
    final bool isActive = _tabController.index == 0;
    final eventFilter = tabsConf[TABS.CLIENTS]['eventFilter'];

    final items = [
      {
        'value': null,
        'label': 'Alfabetic',
        'abbreviation': 'A-Z                  '
      },
      {'value': 'today', 'label': 'Azi', 'abbreviation': 'AZI'},
      {'value': 'tomorrow', 'label': 'Mâine', 'abbreviation': 'MN'},
      {'value': 'this_week', 'label': 'Săpt. curentă', 'abbreviation': 'SC'},
      {'value': 'next_week', 'label': 'Săpt. viitoare', 'abbreviation': 'SV'},
      {'value': 'prev_week', 'label': 'Săpt. trecută', 'abbreviation': 'ST'},
    ];

    return Opacity(
      opacity: isActive ? 1.0 : .5,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          InkWell(
              onTap: () {
                if (_tabController.index != 0) {
                  _tabController.index = 0;
                }
                // UI().currentTab = TABS.CLIENTS;
              },
              child: Container(
                  child:
                      Text('Clienţi', style: TextStyle(color: Colors.white)))),
          SizedBox(width: 12),
          DropdownButton(
              value: eventFilter.state,
              // isExpanded: true,
              iconSize: 24,
              icon: Icon(
                Icons.arrow_drop_down,
                color: Colors.white,
              ),
              underline: SizedBox(height: 0),
              // hint: Icon(Icons.filter_list),
              // disabledHint: Icon(Icons.filter_list),
              selectedItemBuilder: (context) {
                return items
                    .map((i) => Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              i['abbreviation'],
                              textAlign: TextAlign.right,
                              style: TextStyle(color: Colors.white),
                              textScaleFactor: 0.9,
                            ),
                          ],
                        ))
                    .toList();
              },
              items: items
                  .map((i) => DropdownMenuItem(
                      value: i['value'],
                      child: Container(
                          // decoration: BoxDecoration(
                          //     border: Border(
                          //   bottom: BorderSide(color: Colors.grey, width: 0.5),
                          // )),
                          child: Text(
                        i['label'],
                        softWrap: false,
                      ))))
                  .toList(),
              //disabled if null
              onChanged: !isActive
                  ? null
                  : (value) {
                      setState(() {
                        eventFilter.state = value;
                      });
                    }),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    //NOTE need to subscribe
    //maybe because this is stateful?
    //ignore: unused_local_variable
    final m = Provider.of<Models>(context);

    return DefaultTabController(
      length: _tabKeys.length, // This is the number of tabs.
      child: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          // These are the slivers that show up in the "outer" scroll view.
          return <Widget>[
            SliverOverlapAbsorber(
              // This widget takes the overlapping behavior of the SliverAppBar,
              // and redirects it to the SliverOverlapInjector below. If it is
              // missing, then it is possible for the nested "inner" scroll view
              // below to end up under the SliverAppBar even when the inner
              // scroll view thinks it has not been scrolled.
              // This is not necessary if the "headerSliverBuilder" only builds
              // widgets that do not overlap the next sliver.
              handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              sliver: SliverAppBar(
                // title: const Text('Books'), // This is the title in the app bar.

                //NOTE bugfix this fixes the non scrolling list after filtering
                pinned: false,

                //hide the drawer icon
                automaticallyImplyLeading: false,
                // expandedHeight: 50,
                // The "forceElevated" property causes the SliverAppBar to show
                // a shadow. The "innerBoxIsScrolled" parameter is true when the
                // inner scroll view is scrolled beyond its "zero" point, i.e.
                // when it appears to be scrolled below the SliverAppBar.
                // Without this, there are cases where the shadow would appear
                // or not appear inappropriately, because the SliverAppBar is
                // not actually aware of the precise position of the inner
                // scroll views.
                forceElevated: innerBoxIsScrolled,
                flexibleSpace: TabBar(
                  controller: _tabController,
                  // These are the widgets to put in each tab in the tab bar.
                  tabs: _tabKeys.map((TABS k) {
                    if (k == TABS.CLIENTS) {
                      return buildClientTabHeader(context);
                    }
                    return Tab(text: tabsConf[k]['label']);
                  }).toList(),
                  onTap: (int index) {
                    UI().currentTab = _tabKeys.asMap()[index];
                    // tabsConf[UI().currentTab]['query'].state = newQuery;
                  },
                ),
              ),
            ),
          ];
        },
        body: TabBarView(
          //this seems buggy
          physics: NeverScrollableScrollPhysics(),
          dragStartBehavior: DragStartBehavior.start,
          // physics: ClampingScrollPhysics(),
          controller: _tabController,
          // These are the contents of the tab views, below the tabs.
          children: _tabKeys.map((TABS tabKey) {
            if (Models().loading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }

            return SafeArea(
              top: false,
              bottom: false,
              child: Builder(
                // This Builder is needed to provide a BuildContext that is "inside"
                // the NestedScrollView, so that sliverOverlapAbsorberHandleFor() can
                // find the NestedScrollView.
                builder: (BuildContext context) {
                  return WithState(
                    child: ModelList(
                      tabKey: tabKey,
                      key: ValueKey(tabKey.toString()),
                    ),
                    stateContainers:
                        // tabsConf[tabKey]['data'],
                        {
                      "tabKey-query": tabsConf[tabKey]['query'],
                      "$tabKey-selected": tabsConf[tabKey]['selected'],
                      "$tabKey-eventFilter": tabsConf[tabKey]['eventFilter'],
                    },
                  );
                },
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}

class ModelList extends StatefulWidget {
  ModelList({
    Key key,
    this.tabKey,
    // this.data,
  }) : super(key: key);

  final TABS tabKey;
  final prevKey = GlobalKey();
  final currKey = GlobalKey();

  @override
  _ModelListState createState() => _ModelListState();
}

class _ModelListState extends State<ModelList> {
  List<dynamic> data;
  UseState query;
  UseState eventFilter;
  String sel;
  String prevSel;
  Map stateConf;

  @override
  void initState() {
    stateConf = tabsConf[widget.tabKey];
    sel = stateConf['selected'].state;
    prevSel = null;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Map conf = tabsConf[widget.tabKey];
    query = conf['query'];
    eventFilter = conf['eventFilter'];

    switch (widget.tabKey) {
      case TABS.CONTACTS:
        data = Models().contacts;
        break;
      case TABS.CLIENTS:
        data = Models().clients;
        break;
      default:
        break;
    }

    List filteredData = []..addAll(data);

    if (eventFilter.state != null) {
      filteredData = filteredData.where((i) {
        if (i.eventTime == null) {
          return false;
        }
        return dateIsInInterval(eventFilter.state, i.eventTime);
      }).toList();
      filteredData.sort((a, b) => a.eventTime.compareTo(b.eventTime));
    }

    if (query.state != '') {
      var q = query.state.toLowerCase();
      filteredData = filteredData.where((i) {
        return (i.name ?? '').toLowerCase().contains(q) ||
            // (i.email ?? '').toLowerCase().contains(q)||
            (i.phone ?? '').toLowerCase().contains(q);
      }).toList();
    }

    if (filteredData.length == 0) {
      return Center(
        child: DefaultTextStyle(
          style: Theme.of(context).textTheme.display1,
          child: Container(
            padding: EdgeInsets.all(12),
            color: Colors.white,
            alignment: Alignment.center,
            child: Text(
              'Nu sunt persoane de afişat',
              textAlign: TextAlign.center,
            ),
          ),
        ),
      );
    }

    return CustomScrollView(
      shrinkWrap: true,
      physics: AlwaysScrollableScrollPhysics(),
      // The "controller" and "primary" members should be left
      // unset, so that the NestedScrollView can control this
      // inner scroll view.
      // If the "controller" property is set, then this scroll
      // view will not be associated with the NestedScrollView.
      // The PageStorageKey should be unique to this ScrollView;
      // it allows the list to remember its scroll position when
      // the tab view is not on the screen.
      key: makeKey(widget.tabKey.toString()),
      slivers: <Widget>[
        SliverOverlapInjector(
          // This is the flip side of the SliverOverlapAbsorber above.
          handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
        ),
        SliverPadding(
          padding: const EdgeInsets.all(8.0),
          // In this example, the inner scroll view has
          // fixed-height list items, hence the use of
          // SliverFixedExtentList. However, one could use any
          // sliver widget here, e.g. SliverList or SliverGrid.
          sliver: SliverList(
            // The items in this example are fixed to 48 pixels
            // high. This matches the Material Design spec for
            // ListTile widgets.
            // itemExtent: 48.0,

            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                //bottom padding to accomodate the FAB
                if (index == filteredData.length) {
                  return SizedBox(height: 60);
                }

                final item = filteredData[index];

                // This builder is called for each child.
                // In this example, we just number each list item.
                return buildListItem(item, context, filteredData, index);
              },
              // The childCount of the SliverChildBuilderDelegate
              // specifies how many children this inner list
              // has. In this example, each tab has a list of
              // exactly 30 items, but this is arbitrary.
              childCount: filteredData.length + 1,
            ),
          ),
        ),
      ],
    );
  }

  String getEventText(BaseModel m) {
    if (m.eventStatus == EventStatus.NO_EVENT) {
      return '';
    }
    return DateFormat("d MMM \n h:mm a").format(m.eventTime);
  }

  Widget buildButton({Function action, IconData icon, String label}) {
    return FlatButton(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 4.0, vertical: 4.0),
        child: Row(
          children: <Widget>[
            Icon(
              icon,
              color: Colors.white,
            ),
            SizedBox(width: 8),
            Text(
              label,
              maxLines: 1,
              overflow: TextOverflow.fade,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
      color: Colors.teal,
      onPressed: action,
    );
  }

  Widget buildListItem(
      BaseModel item, BuildContext context, List filteredData, int index) {
    var icon = Icons.remove;
    Color itemColor = Colors.grey[700];
    switch (item.notificationStatus) {
      case NotificationStatus.NOTIFICATION_NOT_REQUIRED:
        // TODO: Handle this case.
        break;
      case NotificationStatus.NOTIFICATION_SENT:
        icon = Icons.alarm;
        itemColor = Colors.green;
        break;
      case NotificationStatus.NOTIFICATION_PENDING:
        icon = Icons.alarm;
        itemColor = Colors.black;
        break;
    }
    bool isPrev = prevSel == item.id;
    bool isSelected = sel == item.id;

    //contact
    if (item.ref == null) {
      itemColor = Colors.black;
    }
    //selected
    if (isSelected) {
      itemColor = Colors.teal[800];
    }

    return Container(
      padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
      decoration: BoxDecoration(
          // color:  Colors.grey,
          border: Border(
        // top: BorderSide(color: Colors.grey),
        bottom: BorderSide(color: Colors.grey, width: 0.5),
      )),
      child: Column(
        children: <Widget>[
          InkWell(
            onTap: () {
              setState(() {
                final curr = sel;
                final newSel = item.id;
                if (curr == newSel) {
                  //toggle
                  sel = null;
                  prevSel = curr;
                  stateConf['selected'].setState(sel, notify: false);
                  return;
                }
                if (curr != newSel) {
                  prevSel = curr;
                }
                sel = newSel;
                debugPrint('prev:$prevSel -> curr:$newSel');
                stateConf['selected'].setState(newSel, notify: false);
              });
            },
            child: Container(
              color: Colors.transparent,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Visibility(
                    visible: item.ref != null,
                    child: Container(
                      width: 36.0,
                      padding: EdgeInsets.only(right: 12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            icon,
                            color: itemColor,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          item.name ?? "",
                          textScaleFactor: 1.1 * 1.61,
                          style: TextStyle(
                            height: 1.2,
                            color: itemColor,
                          ),
                        ),
                        Text(
                          item.phone ?? "",
                          textScaleFactor: 1.1,
                          style: TextStyle(fontFamily: "monospace"),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    // width: 35,
                    // color: Colors.red,
                    padding: EdgeInsets.only(left: 12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        // Icon(Icons.event),
                        Text(
                          getEventText(item),
                          textAlign: TextAlign.right,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            // maintainState: true,
            // maintainAnimation: true,
            visible: isSelected || isPrev,
            child: VerticalExpand(
              hide: isPrev,
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    buildButton(
                        label: 'Sună',
                        icon: Icons.call,
                        action: () => launch("tel://${item.phone}")),
                    buildButton(
                        label: 'SMS',
                        icon: Icons.message,
                        action: () => launch("sms://${item.phone}")),
                    buildButton(
                        label: 'Editează',
                        icon: Icons.edit,
                        action: () => pushRoute(context, UpsertPage(), item)),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
