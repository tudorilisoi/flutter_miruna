import 'package:flutter/rendering.dart';
import 'package:package_info/package_info.dart';

import 'changenotifier.dart';

enum TABS {
  CONTACTS,
  CLIENTS,
}

class UI extends LoggedChangeNotifier {
  factory UI() => _instance;

  UI._internal() {
    // init things inside this
  }
  static final UI _instance = UI._internal();

  PackageInfo packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: '?',
    buildNumber: '?',
  );

  TABS _currentTab = TABS.CLIENTS;

  TABS get currentTab => _currentTab;

  set currentTab(TABS currentTab) {
    _currentTab = currentTab;
    debugPrint(currentTab.toString());
    notifyListeners();
  }
}
