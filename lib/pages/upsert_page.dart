import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_miruna/dataservice.dart';
import 'package:flutter_miruna/store/changenotifier.dart';
import 'package:flutter_miruna/store/models/BaseModel.dart';
import 'package:intl/intl.dart';

import '../helpers.dart';

//convert hours to mintes and set alarm text
Map<String, dynamic> recompute(Map<String, dynamic> formValues) {
  // debugPrint("STATE ${currentValue.toString()}");

  final s = new Map<String, dynamic>.from(formValues);
  if (s['notificationHoursBefore'] == null) {
    s['notificationHoursBefore'] = s['notificationMinutesBefore'] / 60;
  } else {
    int hrs = s['notificationHoursBefore'].abs().round();
    s['notificationMinutesBefore'] = hrs * 60;
  }

  if (isDateSet(s['eventTime'])) {
    int hrs = s['notificationHoursBefore'].abs().round();
    DateTime notificationTime =
        s['eventTime'].subtract(new Duration(hours: hrs));
    s['notificationTime'] = notificationTime;

    String alarmText = dfLong.format(notificationTime);

    s['alarmText'] = alarmText;
    debugPrint("@ ${s['eventTime']} -${hrs.toString()} $alarmText");
  } else {
    s['alarmText'] = 'N/A';
  }

  debugPrint('recompute FROM ${formValues.toString()}');
  debugPrint('recompute TO ${s.toString()}');

  return s;
}

class UpsertPage extends StatefulWidget {
  final BaseModel dummyModel = BaseModel.empty();
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  _UpsertPageState createState() => _UpsertPageState();
}

class _UpsertPageState extends State<UpsertPage>
    with AfterLayoutMixin<UpsertPage> {
  Map<String, dynamic> initialState;
  bool isValid = true;
  int resetSeq = 0;
  bool _showSMSPreview = false;
  String notificationText = '';

  // TODO update this with setState and disable the submit
  bool formValid = false;
  // final GlobalKey<FormFieldState> _specifyTextFieldKey =
  //     GlobalKey<FormFieldState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    // we now have context
    final BaseModel model = ModalRoute.of(context).settings.arguments;
    initialState = model.toMap();
    setState(() {
      initialState = recompute(initialState);
      resetSeq++;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  FormBuilderState get currentState {
    return widget._fbKey.currentState;
  }

  Map get currentValue {
    return widget._fbKey.currentState != null
        ? widget._fbKey.currentState.value
        : initialState;
  }

  String getSMSPreview() {
    final s = currentValue;
    widget.dummyModel.fromMap(s);
    return widget.dummyModel.notificationText;
  }

  String get alarmText {
    var s = recompute(currentValue);
    return s['alarmText'];
  }

  @override
  Widget build(BuildContext context) {
    // Use the Todo to create the UI.
    return Scaffold(
      key: widget._scaffoldKey,
      appBar: AppBar(
        title: Text('Programare'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            DefaultTextStyle(
              style: Theme.of(context).textTheme.display1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox(
                    width: 48,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[],
                    ),
                  ),
                  Expanded(
                      child: Text(
                    initialState != null && currentValue['name'] != ''
                        ? currentValue['name']
                        : "¯\\_(ツ)_/¯",
                    key: ValueKey('heroText-$resetSeq'),
                    softWrap: true,
                    textAlign: TextAlign.center,
                  )),
                  SizedBox(
                    width: 48,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        IconButton(
                          padding: EdgeInsets.all(4.0),
                          iconSize: 30,
                          icon: Icon(Icons.delete),
                          color: Colors.red,
                          // tooltip: 'Increase volume by 10',
                          onPressed: () async {
                            final BaseModel model =
                                ModalRoute.of(context).settings.arguments;
                            if (model.ref == null) {
                              await ackDialog(
                                context,
                                title: 'Info',
                                content:
                                    'Această persoană nu este în lista de pacienţi',
                              );
                              return;
                            }
                            final confirmValue = await asyncConfirmDialog(
                              context,
                              title: 'Confirmaţi ştergerea?',
                              content:
                                  'NOTĂ: se şterge din lista de pacienţi, rămâne în agendă',
                            );
                            if (confirmValue == ConfirmAction.CANCEL) {
                              return;
                            }
                            await DataService.deleteClient(model)
                                .catchError((_) {
                              widget._scaffoldKey.currentState.showSnackBar(
                                  SnackBar(
                                      content: Text('Eroare, reîncercaţi!')));
                            });
                            widget._scaffoldKey.currentState.showSnackBar(
                                SnackBar(
                                    content: Text('Pacientul a fost şters')));
                            await DataService.initialize();
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: initialState == null ? null : buildForm(context),
              ),
            ),
          ],
        ),
      ),
    );
  }

  FormBuilder buildForm(BuildContext context) {
    return FormBuilder(
      onChanged: (values) {
        if (currentState.saveAndValidate()) {
          debugPrint("VALID");
          setState(() {
            isValid = true;
          });
        } else {
          debugPrint("INVALID");
          setState(() {
            isValid = false;
          });
        }
        debugPrint("form changed ${values.toString()}");

        // recompute(formState);
      },
      // context,
      key: widget._fbKey,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      initialValue: initialState,
      child: Column(
        children: <Widget>[
          FormBuilderTextField(
            key: ValueKey("name-$resetSeq"),
            attribute: "name",
            decoration: InputDecoration(
              labelText: "Nume şi prenume",
            ),
            // onChanged: (val) => _onChanged("name", val,
            //     values: currentState.value),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            initialValue: initialState['name'],
            validators: [
              FormBuilderValidators.required(
                  errorText: "Trebuie să completaţi numele"),
              FormBuilderValidators.minLength(3,
                  errorText: "Numele trebuie să aibă cel puţin 3 litere"),
            ],
            keyboardType: TextInputType.text,
          ),
          FormBuilderTextField(
            key: ValueKey("phone-$resetSeq"),
            attribute: "phone",
            decoration: InputDecoration(
              labelText: "Nr. de telefon",
            ),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            initialValue: initialState['phone'],
            validators: [
              FormBuilderValidators.minLength(10,
                  errorText:
                      "Nr. de telefon trebuie să aibă cel puţin 10 cifre"),
              FormBuilderValidators.required(
                  errorText: "Trebuie să completaţi nr. de telefon"),
            ],
            keyboardType: TextInputType.phone,
          ),
          FormBuilderDateTimePicker(
            format: dfLong,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            attribute: "eventTime",
            validators: [
              // FormBuilderValidators.required(
              //     errorText:
              //         "Trebuie să completaţi data programării"),
            ],
            inputType: InputType.both,
            decoration: InputDecoration(labelText: "Data şi ora programării"),
            // readonly: true,
          ),
          FormBuilderSlider(
            key: ValueKey("notificationHoursBefore-$resetSeq"),
            attribute: "notificationHoursBefore",
            validators: [
              FormBuilderValidators.min(-24),
              FormBuilderValidators.max(-1),
            ],
            // onChanged: (val) => _onChanged(
            //     "notificationHoursBefore", val,
            //     values: currentState.value),
            min: -24,
            max: -1,
            initialValue: -1 * initialState['notificationHoursBefore'],
            divisions: 24,
            numberFormat: NumberFormat("0"),
            // activeColor: Colors.red,
            // inactiveColor: Colors.pink[100],
            decoration: InputDecoration(
              labelText: "Notificare prin SMS (ore în avans)",
            ),
          ),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: Row(
                  children: <Widget>[
                    SizedBox(width: 30, child: Icon(Icons.alarm)),
                    React((_) => Text(
                          alarmText,
                          key: ValueKey(currentValue['notificationText']),
                          textAlign: TextAlign.left,
                          textScaleFactor: 1.15,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )),
                  ],
                ),
              ),
            ],
          ),
          buildExpansionTile(),
          Row(
            children: <Widget>[
              Expanded(
                child: MaterialButton(
                  disabledColor: Colors.grey,
                  color: Theme.of(context).accentColor,
                  child: Text(
                    "Salvează",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: !isValid
                      ? null
                      : () async {
                          if (currentState.saveAndValidate()) {
                            print(currentValue);
                            final BaseModel model =
                                ModalRoute.of(context).settings.arguments;

                            model.fromMap(recompute(currentValue));
                            model.notificationSent = false;
                            bool success =
                                await DataService.upsertClient(model);

                            // Models().notifyListeners();
                            final String msg = success
                                ? 'Informaţiile au fost salvate'
                                : 'Nu s-au putut salva datele';
                            widget._scaffoldKey.currentState
                                .showSnackBar(SnackBar(content: Text(msg)));
                          } else {
                            print(currentState.value);
                            print("validation failed");
                          }
                          //must match contacts against clients
                          await DataService.initialize();
                        },
                ),
              ),
              SizedBox(
                width: 20,
              ),
              Expanded(
                child: MaterialButton(
                  color: Theme.of(context).accentColor,
                  child: Text(
                    "Reset",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    setState(() {
                      resetSeq++;
                      currentState.reset();
                    });
                  },
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget buildExpansionTile() {
    final smsLength = _showSMSPreview ? " (${getSMSPreview().length})" : "";
    final smsLeading =
        !_showSMSPreview ? "[Edit] Textul SMSului" : "[Preview] Textul SMSului";
    final smsTitle = '$smsLeading $smsLength';
    return ExpansionTile(
      leading: Switch(
          onChanged: (bool value) {
            setState(() {
              _showSMSPreview = value;
            });
          },
          value: _showSMSPreview),
      title: Text(smsTitle),
      children: <Widget>[
        Visibility(
            visible: _showSMSPreview,
            child: React((_) => Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: Text(getSMSPreview()),
                ))),
        Visibility(
          visible: !_showSMSPreview,
          child: FormBuilderTextField(
            key: ValueKey("smsTemplate-$resetSeq"),
            attribute: "smsTemplate",
            // decoration: InputDecoration(
            //   labelText: "Nr. de telefon",
            // ),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            initialValue: initialState['smsTemplate'],
            validators: [
              FormBuilderValidators.minLength(10,
                  errorText: "Trebuie să completaţi textul SMSului"),
              FormBuilderValidators.required(
                  errorText: "Trebuie să completaţi textul SMSului"),
            ],
            keyboardType: TextInputType.text,
          ),
        ),
      ],
    );
  }
}
