import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_miruna/store/db.dart';
import 'package:flutter_miruna/store/models.dart';
import 'package:flutter_miruna/store/models/BaseModel.dart';

import 'helpers.dart';

class DataService {
  static Future<bool> upsertClient(BaseModel model) async {
    bool ret;
    final lastID = model.id;
    if (!model.isNew && lastID != model.phoneNumberToID()) {
      await deleteClient(model);
      model.ref = null;
    }
    model.id = model.phoneNumberToID();
    if (model.isNew) {
      ret = await saveClient(model);
    } else {
      ret = await updateClient(model);
    }
    Models().notifyListeners();
    return ret;
  }

  static Future deleteClient(BaseModel model) async {
    await Firestore.instance.runTransaction((Transaction tx) async {
      DocumentSnapshot snapshot = await tx.get(model.ref);
      await tx.delete(snapshot.reference);
    });
    Models().removePerson(model);
    model.ref = null;
  }

  static Future<bool> saveClient(BaseModel model) async {
    debugPrint("CREATE ${model.toMap().toString()}");
    Map<String, dynamic> data = model.toDocumentData();
    final DocumentReference contactRef = db.document('clients/${data['id']}');
    bool ret = false;
    try {
      await contactRef.setData(data).then((ref) {
        model.ref = contactRef;
        debugPrint('Save success!');
        ret = true;
      });
    } catch (e) {
      debugPrint('Save Error: ${e.toString()}');
      ret = false;
    }
    return ret;
  }

  static Future<bool> simpleUpdate(BaseModel model) async {
    return model.ref.updateData(model.toDocumentData()).then((_) {
      debugPrint('UPDATE OK ${model.id}');
      // Models().notifyListeners();
      return true;
    }).catchError((e) {
      debugPrint('UPDATE FAILED ${model.id}');
      return false;
    });
  }

  static Future<bool> updateClient(BaseModel model) async {
    debugPrint("UPDATE ${model.toMap().toString()}");
    bool ret = false;
    try {
      await db.runTransaction((Transaction tx) async {
        DocumentSnapshot latest = await tx.get(model.ref);
        // if (latest.exists) {
        await tx.update(model.ref, model.toDocumentData());
        // }
      });
      ret = true;
      debugPrint('Update success!');
    } catch (e) {
      ret = false;
      debugPrint('Update Error: ${e.toString()}');
    }
    return ret;
  }

  static void setupFBSync() {
    var userQuery = db.collection('clients').orderBy('name');
    userQuery.snapshots().listen((data) {
      debugPrint(' setupFBSync got FB ${data.documents.length} updates');
      List<BaseModel> list =
          data.documents.map((d) => BaseModel.empty().fromDocument(d)).toList();
      Models().clients = list;
      // TODO replace matching contacts in Models().contacts by contactIdentifier
    });
    // return list;
  }

  static Future<List<BaseModel>> loadClientModelsByQuery(Query query) async {
    List<BaseModel> ret = [];
    await query.getDocuments().then((data) {
      print('FB data found ${data.documents.length} items');
      if (data.documents.length > 0) {
        List<BaseModel> list = data.documents
            .map((d) => BaseModel.empty().fromDocument(d))
            .toList();
        ret = list;
      } else {
        print('FB data not found');
      }
    });
    return ret;
  }

  static Future<List<BaseModel>> loadClientModelsForNotification() async {
    // return Future.delayed(Duration(seconds: 2), () => mockContacts);
    var query = db
        .collection('clients')
        .where('eventTime', isGreaterThan: unixNow())
        .orderBy('eventTime');
    return loadClientModelsByQuery(query).catchError((e) {
      debugPrint('FB LOAD ERROR ${e.toString()}');
      Models().error = e.toString();
      return [];
    });
  }

  static Future<List<BaseModel>> loadClientModels() async {
    // return Future.delayed(Duration(seconds: 2), () => mockContacts);
    var query = db.collection('clients').orderBy('name');
    Models().error = null;
    return loadClientModelsByQuery(query).catchError((e) {
      debugPrint('FB LOAD ERROR ${e.toString()}');
      Models().error = e.toString();
      return [];
    });
  }

  //NOTE FB contacts should be loaded
  static Future<List<BaseModel>> loadPhoneContacts() async {
    final uid = await deviceUUID();
    Iterable<Contact> contacts =
        await ContactsService.getContacts(withThumbnails: false);
    return contacts
        .map((c) => BaseModel.empty().fromPhoneContact(c, uid))
        .toList();
  }

  static Future<void> initialize() async {
    debugPrint('initialize');

    Models().loading = true;
    Models().empty();
    try {
      //just in case the tree is building

      var clients = await loadClientModels();
      Models().clients = clients;

      var pc = await loadPhoneContacts();
      Models().contacts = pc;
      await Future.delayed(const Duration(milliseconds: 300), () => {});
    } catch (e) {}
    
    Models().loading = false;
  }

  static void reloadData() {}
}
