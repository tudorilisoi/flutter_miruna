
import 'models/BaseModel.dart';

List<BaseModel> mockClients = [
  new BaseModel(
       phone: '004443555555', email: 'a3a@x.com', name: 'Alpha Channel'),
  new BaseModel(
       phone: '004343455555', email: 'aa@x.com', name: 'Zeta Channel'),
  new BaseModel(
       phone: '0065656565622', email: 'apa@x.com', name: 'Beta Channel'),
  new BaseModel(
       phone: '00443333433333', email: 'ai@x.com', name: 'Coco Channel'),
];

List<BaseModel> mockContacts = [
  new BaseModel(
       phone: '002443922222', email: 'ag@x.com', name: 'Alpha Centauri'),
  new BaseModel(
       phone: '004323422222', email: 'af@x.com', name: 'Zeta Reticuli'),
  new BaseModel(
       phone: '00656565656', email: 'az@x.com', name: 'Boo boo'),
  new BaseModel(
       phone: '0044388333422', email: 'ac@x.com', name: 'Arnold Bohr'),
  new BaseModel(
      
      phone: '004433334',
      email: 'aa@x.com',
      name: 'Arnold Steinberg'),
  new BaseModel(
       phone: '004433334', email: 'aa@x.com', name: 'Niels Meyer'),
];
