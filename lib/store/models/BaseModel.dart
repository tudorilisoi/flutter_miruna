// import 'package:flutter/material.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_miruna/settingservice.dart';
import 'package:shortid/shortid.dart';

import '../../helpers.dart';

final defaultNotificationMinutes = 120;

enum EventStatus {
  NO_EVENT,
  EVENT_PENDING,
  EVENT_IN_THE_PAST,
}
enum NotificationStatus {
  NOTIFICATION_NOT_REQUIRED,
  NOTIFICATION_SENT,
  NOTIFICATION_PENDING,
}

class BaseModel {
  BaseModel({
    this.name,
    this.email,
    this.phone,
    this.eventTime,
    this.notificationTime,
  });

  // empty constructor
  BaseModel.empty() {
    this.name = '';
    this.phone = '';
    this.smsTemplate = SettingsService().notificationTemplate;
    this.notificationMinutesBefore = defaultNotificationMinutes;
    this.notificationSent = false;
  }

  String id = () {
    shortid.characters('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
    return shortid.generate();
  }();

  List<String> phones;
  DocumentReference ref;
  String name;
  String phone;
  String email;
  String smsTemplate;
  String contactIdentifier;
  DateTime eventTime;
  DateTime notificationTime;
  int notificationMinutesBefore;
  bool notificationSent;

  bool get isNew {
    return ref == null;
  }

  static List<String> get keys {
    final fields = [
      'id',

      //from Contact
      'contactIdentifier',
      'name',
      'email',
      'smsTemplate',
      'phone',
      'eventTime',
      'notificationTime',
      'notificationMinutesBefore',
      'notificationSent',
    ];
    return fields;
  }

  BaseModel fromDocument(DocumentSnapshot d) {
    ref = d.reference;
    id = d.documentID;
    return this.fromMap(d.data);
  }

  String _findPhone(Iterable phones) {
    for (Map i in phones) {
      if (i['value'] != null && i['value'] != '') {
        // debugPrint('found phone ${i.toString()}');
        return i['value'];
      }
    }
    return '';
  }

  BaseModel fromPhoneContact(Contact c, String uuidPrefix) {
    final cmap = c.toMap();

    // debugPrint('contact');
    debugPrint('${cmap['displayName']}');
    // debugPrint('${cmap['phones'].toString()}');
    // id = c.hashCode.toString();
    BaseModel ret = this.fromMap({
      'phone': _findPhone(cmap['phones']),
      'name': cmap['displayName'],
      'notificationTime': null,
    });
    ret.contactIdentifier = cmap['identifier'];
    // id = uuidPrefix + '_' + this.contactIdentifier;
    id = ret.phoneNumberToID();
    return ret;
  }

  final _re = RegExp(r"[^\d]+");
  String phoneNumberToID() {
    // String ret = removeSpaces('phone_' + this.phone);
    return phone.replaceAll(_re, '');
  }

  Map<String, dynamic> toDocumentData() {
    Map<String, dynamic> data = this.toMap();
    data['eventTime'] = dateToUnix(data['eventTime']);
    data['notificationTime'] = dateToUnix(data['notificationTime']);
    return data;
  }

  BaseModel fromMap(Map<String, dynamic> m) {
    // this.id = m['id'];
    this.name = m['name'];
    this.email = m['email'];
    this.phone = m['phone'];
    if (m['smsTemplate'] != null && m['smsTemplate'] != '') {
      this.smsTemplate = m['smsTemplate'];
    }
    this.contactIdentifier = m['contactIdentifier'];
    this.eventTime = convertToDate(m['eventTime']);
    this.notificationTime = convertToDate(m['notificationTime']);
    if (m['notificationMinutesBefore'] != null) {
      this.notificationMinutesBefore =
          m['notificationMinutesBefore'].abs().round();
    }

    this.notificationSent = m['notificationSent'] ?? false;
    return this;
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'id': this.id,
      'name': this.name,
      'email': this.email,
      'phone': this.phone,
      'smsTemplate': this.smsTemplate,
      'contactIdentifier': this.contactIdentifier,
      'eventTime': this.eventTime,
      'notificationTime': this.notificationTime,
      'notificationMinutesBefore': this.notificationMinutesBefore,
      'notificationSent': this.notificationSent,
    };
    return map;
  }

  String get notificationText {
    String s = '${this.smsTemplate}';
    s = s.replaceAll('[nume]', this.name);
    s = s.replaceAll('[data]',
        this.eventTime == null ? 'N/A' : dfLong.format(this.eventTime));
    return s;
  }

  EventStatus get eventStatus {
    final now = DateTime.now().toUtc();
    if (this.eventTime == null) {
      return EventStatus.NO_EVENT;
    }
    if (now.isAfter(this.eventTime)) {
      return EventStatus.EVENT_IN_THE_PAST;
    } else {
      return EventStatus.EVENT_PENDING;
    }
  }

  NotificationStatus get notificationStatus {
    if (this.eventStatus != EventStatus.EVENT_PENDING) {
      return NotificationStatus.NOTIFICATION_NOT_REQUIRED;
    }
    if (this.notificationSent == true) {
      return NotificationStatus.NOTIFICATION_SENT;
    }
    return NotificationStatus.NOTIFICATION_PENDING;
  }

  bool get shouldNotify {
    if (this.notificationSent == true) {
      return false;
    }
    if (this.eventTime == null) {
      return false;
    }
    final now = DateTime.now().toUtc();
    if (now.isAfter(this.eventTime)) {
      debugPrint('TOO LATE TO NOTIFY ${this.name}');
      return false;
    }
    if (now.isAfter(this.notificationTime)) {
      debugPrint('SHOULD NOTIFY ${this.name}');
      return true;
    } else {
      debugPrint('TOO EARLY TO NOTIFY ${this.name}');
    }
    return false;
  }
}
