import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_miruna/dataservice.dart';
import 'package:flutter_miruna/pages/settings_page.dart';
import 'package:flutter_miruna/settingservice.dart';
import 'package:flutter_miruna/smsservice.dart';
import 'package:flutter_miruna/store/db.dart';
import 'package:flutter_miruna/store/models/BaseModel.dart';
import 'package:flutter_miruna/store/ui.dart';
import 'package:flutter_miruna/widgets/home_page_tabs.dart';
import 'package:flutter_miruna/pages/upsert_page.dart';
import 'package:permission_handler/permission_handler.dart';

import '../helpers.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with AfterLayoutMixin<MyHomePage> {
  TextEditingController _searchQueryController = TextEditingController();
  bool _isSearching = false;
  String searchQuery = "Search query";
  final FocusNode _queryFocus = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  void changeTextOnTabChange() {
    debugPrint('TAB CHANGE');
    _queryFocus.unfocus();
  }

  checkPermissions() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.contacts,
      Permission.sms,
      Permission.phone,
    ].request();
    debugPrint('PERMISSIONS ${statuses.toString()}');

    bool hasPermissions = true;
    statuses.forEach((permission, status) {
      if (status != PermissionStatus.granted) {
        hasPermissions = false;
      }
    });

    if (!hasPermissions) {
      await ackDialog(
        context,
        title: 'Info',
        content:
            'E necesar să accordaţi toate permisiunile.\nReporniţi aplicaţia',
      );

      //exit app
      SystemNavigator.pop();
    }
  }

  init() async {
    debugPrint('**** HomePage init');
    try {
      await SettingsService().loadSettings();
    } catch (e) {
      debugPrint('init() CANNOT LOAD SETTINGS');
    }
    await checkPermissions();
    await firestoreSignIn();
    DataService.setupFBSync();
    DataService.initialize();
    UI().addListener(this.changeTextOnTabChange);
  }

  @override
  void afterFirstLayout(BuildContext context) {
    // we now have context
    init();
  }

  void addItem() {
    final item = BaseModel.empty();
    final s = item.toMap();
    s['name'] = '';
    s['phone'] = '';
    s['notificationHoursBefore'] = s['notificationMinutesBefore'] / 60;
    s['smsTemplate'] = SettingsService().notificationTemplate;

    debugPrint("***INIT DONE");
    pushRoute(context, UpsertPage(), item);
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the addItem method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        leading: null,
        // leading: _isSearching ? Container() : Container(),
        // leading: _isSearching ? const BackButton() : Container(),
        title: _isSearching ? _buildSearchField() : _buildTitle(context),
        actions: _buildActions(),
      ),
      body: HomePageTabs(),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
              child: ListView(
                // Important: Remove any padding from the ListView.
                padding: EdgeInsets.zero,
                children: <Widget>[
                  DrawerHeader(
                    child: Text('AutoReminders'),
                    decoration: BoxDecoration(
                      color: Colors.blueGrey,
                    ),
                  ),
                  ListTile(
                      title: Text('Reîncarcă datele'),
                      onTap: () async {
                        await DataService.initialize();
                        SMSService.sendDueNotifications();
                        Navigator.of(context).pop();
                        // Update the state of the app.
                        // ...
                      }),
                  ListTile(
                    title: Text('Setări'),
                    onTap: () async {
                      Navigator.of(context).pop();
                      pushRoute(context, SettingsPage(), {});
                      // Update the state of the app.
                      // ...
                    },
                  ),
                ],
              ),
            ),
            Align(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                    'version ${UI().packageInfo.version} build ${UI().packageInfo.buildNumber}'),
              ),
              alignment: FractionalOffset.bottomRight,
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: addItem,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget _buildSearchField() {
    return TextField(
      focusNode: _queryFocus,
      controller: _searchQueryController,
      // autofocus: _isSearching,
      autofocus: true,
      decoration: InputDecoration(
        hintText: "Căutare",
        border: InputBorder.none,
        hintStyle: TextStyle(color: Colors.white30),
      ),
      style: TextStyle(color: Colors.white, fontSize: 16.0),
      onChanged: (query) => updateSearchQuery(query),
    );
  }

  List<Widget> _buildActions() {
    if (_isSearching) {
      return <Widget>[
        IconButton(
          icon: const Icon(Icons.clear),
          onPressed: () {
            _stopSearching();
          },
        ),
      ];
    }

    return <Widget>[
      IconButton(
        icon: const Icon(Icons.search),
        onPressed: _startSearch,
      ),
    ];
  }

  void updateSearchQuery(String newQuery) {
    debugPrint('query: $newQuery');
    tabsConf[TABS.CLIENTS]['query'].state = newQuery;
    tabsConf[TABS.CONTACTS]['query'].state = newQuery;
    setState(() {
      searchQuery = newQuery;
    });
  }

  void _stopSearching() {
    _clearSearchQuery();

    setState(() {
      _isSearching = false;
    });
  }

  void _clearSearchQuery() {
    setState(() {
      _searchQueryController.clear();
      updateSearchQuery("");
    });
  }

  void _startSearch() {
    // ModalRoute.of(context)
    //     .addLocalHistoryEntry(LocalHistoryEntry(onRemove: _stopSearching));

    setState(() {
      _isSearching = true;
    });
  }

  _buildTitle(BuildContext context) {
    final tabLabel = tabsConf[UI().currentTab]['label'];
    return Text('AutoReminders');
  }
}
