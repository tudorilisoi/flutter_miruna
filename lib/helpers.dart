import 'dart:convert';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';

final DateFormat dfLong = DateFormat("EEEE, d MMMM yyyy 'la' h:mm a");
final DateFormat dfShort = DateFormat("d/M yyyy h:mm a");

bool isDateSet(DateTime d) {
  return d.toString() != null.toString();
}

pushRoute(BuildContext context, Widget w, dynamic data) {
  return Navigator.push(
    context,
    MaterialPageRoute(
      builder: (context) => w,
      // Pass the arguments as part of the RouteSettings. The
      // DetailScreen reads the arguments from these settings.
      settings: RouteSettings(
        arguments: data,
      ),
    ),
  );
}

int dateToUnix(DateTime d) {
  if (!isDateSet(d)) {
    return null;
  }
  return (d.toUtc().millisecondsSinceEpoch);
}

int unixNow() {
  return DateTime.now().toUtc().millisecondsSinceEpoch;
}

DateTime convertToDate(value) {
  if (value is int) {
    return DateTime.fromMillisecondsSinceEpoch(value, isUtc: true).toLocal();
  }
  if (value is DateTime) {
    return value;
  }
  return null;
}

/// Returns the difference (in full days) between the provided date and today.
int dateDiffInDays(DateTime date) {
  DateTime now = DateTime.now();
  return DateTime(date.year, date.month, date.day)
      .difference(DateTime(now.year, now.month, now.day))
      .inDays;
}

int weekNumber(DateTime date) {
  int dayOfYear = int.parse(DateFormat("D").format(date));
  return ((dayOfYear - date.weekday + 10) / 7).floor();
}

/// @intervalName is a string descibing the interval such as 'tomorrow' or 'next week'
bool dateIsInInterval(String intervalName, DateTime date) {
  DateTime now = DateTime.now();
  switch (intervalName) {
    case 'this_week':
      return weekNumber(date) == weekNumber(now);
      break;
    case 'next_week':
      return weekNumber(date) == weekNumber(now) + 1;
      break;
    case 'prev_week':
      return weekNumber(date) == weekNumber(now) - 1;
      break;
    case 'today':
      return dateDiffInDays(date) == 0;
      break;
    case 'tomorrow':
      return dateDiffInDays(date) == 1;
      break;
    case 'yesterday':
      return dateDiffInDays(date) == -1;
      break;
    default:
      throw 'bad intervalName $intervalName';
      break;
  }
}

String removeSpaces(inputString) {
  return inputString.replaceAll(new RegExp(r"\s+\b|\b\s"), '');
}

Future<String> deviceUUID() async {
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  // if (Theme.of(context).platform == TargetPlatform.iOS) {
  //   IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
  //   return iosDeviceInfo.identifierForVendor; // unique ID on iOS
  // } else {
  AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
  return androidDeviceInfo.androidId; // unique ID on Android
  // }
}

enum ConfirmAction { CANCEL, ACCEPT }

Future<void> ackDialog(BuildContext context, {title: 'Info', content: ''}) {
  return showDialog<void>(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(title),
        content: Text(content),
        actions: <Widget>[
          FlatButton(
            child: Text('OK'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

Future<ConfirmAction> asyncConfirmDialog(BuildContext context,
    {title: 'Confirmaţi', content: ''}) async {
  return showDialog<ConfirmAction>(
    context: context,
    barrierDismissible: false, // user must tap button for close dialog!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(title),
        content: Text(content),
        actions: <Widget>[
          FlatButton(
            child: const Text('Nu'),
            onPressed: () {
              Navigator.of(context).pop(ConfirmAction.CANCEL);
            },
          ),
          FlatButton(
            child: const Text('Da'),
            onPressed: () {
              Navigator.of(context).pop(ConfirmAction.ACCEPT);
            },
          )
        ],
      );
    },
  );
}

class FileHelper {
  static Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  static Future<File> localFile(String filename, {create = false}) async {
    final path = await _localPath;
    final File ret = File('$path/${filename}');
    if (create) {
      await ret.create();
    }
    return ret;
  }

  static Future<Map<String, dynamic>> loadJSON(String filename) async {
    try {
      final contents = await loadFile(filename);
      return contents == null ? null : jsonDecode(contents);
    } catch (e) {
      debugPrint(e.toString());
      return null;
    }
  }

  static Future<String> loadFile(String filename) async {
    try {
      final File file = await localFile(filename);
      final String contents = await file.readAsString();
      final String fPath = file.path;
      debugPrint('loadFile($fPath)');
      debugPrint('loadFile() loaded file: $contents');
      return contents;
    } catch (e) {
      debugPrint('loadFile() ERROR: ' + e.toString());
      return null;
    }
  }

  static Future<bool> saveJSON(
      Map<String, dynamic> jsonObj, String filename) async {
    final File file = await localFile(filename);
    try {
      final File res = await file.writeAsString(jsonEncode(jsonObj));
      debugPrint('saveJSON(): saved ${file.path}');
      return true;
    } catch (e) {
      debugPrint('saveJSON(): ERR writing ${file.path}');
      throw e;
    }
  }
}
