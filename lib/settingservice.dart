import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter_miruna/helpers.dart';
import 'package:flutter_miruna/store/changenotifier.dart';
import 'dart:convert';

import 'package:path_provider/path_provider.dart';

class SettingsService extends LoggedChangeNotifier {
  static final SettingsService _instance = SettingsService._internal();
  factory SettingsService() => _instance;

  static final Map<String, dynamic> defaults = {
    'testMode': true,
    'sendNotifications': false,
    'testPhoneNumber': '0737 282 174',
    'notificationTemplate':
        "Buna ziua! [nume], va reamintim ca aveti programare [data] la cabinetul stomatologic Doctor Smile!",
  };

  SettingsService._internal() {
    // init things inside this
    testMode = defaults['testMode'];
    sendNotifications = defaults['sendNotifications'];
    testPhoneNumber = defaults['testPhoneNumber'];
    notificationTemplate = defaults['notificationTemplate'];
    _loaded = false;
  }

  // wether the settings were loaded from the json file
  bool _loaded;
  bool get loaded {
    return _loaded;
  }

  bool sendNotifications;

  bool _testMode;

  bool get testMode => _testMode;

  set testMode(bool testMode) {
    _testMode = testMode;
    notifyListeners();
  }

  String _testPhoneNumber;

  String get testPhoneNumber => _testPhoneNumber;

  set testPhoneNumber(String testPhoneNumber) {
    _testPhoneNumber = testPhoneNumber;
    notifyListeners();
  }

  String _notificationTemplate;

  String get notificationTemplate => _notificationTemplate;

  set notificationTemplate(String notificationTemplate) {
    _notificationTemplate = notificationTemplate;
    notifyListeners();
  }

  void fromMap(Map<String, dynamic> map) {
    testMode = map['testMode'];
    sendNotifications = map['sendNotifications'] ?? false;
    testPhoneNumber = map['testPhoneNumber'];
    notificationTemplate = map['notificationTemplate'];
  }

  Map<String, dynamic> toMap() {
    final map = {
      'testMode': testMode,
      'sendNotifications': sendNotifications,
      'testPhoneNumber': testPhoneNumber,
      'notificationTemplate': notificationTemplate,
    };
    return map;
  }

  String toJSON() {
    final map = toMap();
    return jsonEncode(map);
  }

  void fromJSON(str) {
    final map = jsonDecode(str);
    fromMap(map);
  }

  Future<void> saveSettings() async {
    await FileHelper.saveJSON(toMap(), 'settings.json');
  }

  Future<void> loadSettings() async {
    final obj = await FileHelper.loadJSON('settings.json');
    fromMap(obj);
  }
}
