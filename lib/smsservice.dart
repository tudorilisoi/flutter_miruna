import 'dart:io';

import 'package:diacritic/diacritic.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_miruna/dataservice.dart';
import 'package:flutter_miruna/helpers.dart';
import 'package:flutter_miruna/settingservice.dart';
import 'package:flutter_miruna/store/models/BaseModel.dart';
import 'package:intl/date_symbol_data_local.dart' as df;
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:logger/src/outputs/file_output.dart';
import 'package:sms_maintained/sms.dart';

class SMSService {
  static Logger logger;
  static Future<void> log(String s) async {
    if (logger == null) {
      final File file = await FileHelper.localFile('sms_log.txt');
      logger = Logger(
        printer: SimplePrinter(printTime: true),
        output: FileOutput(file: file, overrideExisting: false),
      );
    }
    debugPrint(s);
    logger.d('$s\n');
  }

  static Future<void> sendSMS(BaseModel model) async {
    final testMode = SettingsService().testMode;
    final testPhone = SettingsService().testPhoneNumber;
    SmsSender sender = new SmsSender();
    String address = testMode ? testPhone : model.phone;
    address = address.replaceAll(new RegExp(r"\s+\b|\b\s"), '');

    log('***sendMS() starts');
    model.notificationSent = true;
    // NOTE cannot await in offline mode
    // @see https://stackoverflow.com/a/53551036
    DataService.simpleUpdate(model);

    // bool success = await DataService.simpleUpdate(model);

    // if (!success) {
    //   log("[${model.name}] FAILED TO UPDATE 'notificationSent'");
    //   return;
    // }
    log('[${model.name}] Sending SMS...[${address}]');
    final String messageText = removeDiacritics(model.notificationText);
    SmsMessage message = new SmsMessage(address, messageText);
    log('[${messageText}]');
    message.onStateChanged.listen((state) async {
      if (state == SmsMessageState.Sent) {
        log("[${model.name}] SMS is sent!");
      } else if (state == SmsMessageState.Delivered) {
        log("[${model.name}] SMS is delivered!");
      } else if (state == SmsMessageState.Fail) {
        log("[${model.name}] SMS FAILED!");
        //revert sent status
        model.notificationSent = false;
        DataService.simpleUpdate(model);
        // bool success = await DataService.simpleUpdate(model);
        // if (!success) {
        //   log("[${model.name}] FAILED TO REVERT 'notificationSent'");
        //   return;
        // }
      }
    });
    sender.sendSms(message);
  }

  static void sendDueNotifications() async {
    try {
      await SettingsService().loadSettings();
    } catch (e) {
      log('sendDueNotifications() CANNOT LOAD SETTINGS');
      return;
    }

    final sendNotifications = SettingsService().sendNotifications;
    Intl.defaultLocale = 'ro_RO';
    await df.initializeDateFormatting('ro_RO');
    log('STARTING sendDueNotifications @ ${dfLong.format(DateTime.now())} ');
    if (!sendNotifications) {
      log('sendNotifications is turned OFF, exiting');
      return;
    }
    List<BaseModel> models =
        await DataService.loadClientModelsForNotification();
    models.forEach((m) {
      if (m.shouldNotify) {
        //send the actual sms
        sendSMS(m);
      }
    });

    final String str = await FileHelper.loadFile('sms_log.txt');
    debugPrint('\n*** LOG\n${str}\n*** END LOG\n');
  }
}
