import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:simple_animations/simple_animations.dart';

class VerticalExpand extends StatefulWidget {
  VerticalExpand(
      {Key key,
      @required this.child,
      this.hide = false,
      this.onDismissed,
      this.onCompleted})
      : super(key: key);
  Widget child;
  bool hide;
  Function onDismissed;
  Function onCompleted;

  @override
  _VerticalExpandState createState() => _VerticalExpandState();
}

class _VerticalExpandState extends State<VerticalExpand>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  BuildContext ctx;
  @override
  Widget build(BuildContext context) {
    ctx = context;
    return ControlledAnimation(
      animationControllerStatusListener: (status) {
        if ((widget.onDismissed != null) &&
            status == AnimationStatus.dismissed) {
          debugPrint('${widget.hide}: $status');
          widget.onDismissed();
        }
        if ((widget.onCompleted != null) &&
            status == AnimationStatus.completed) {
          debugPrint('${widget.hide}: $status');
          widget.onCompleted();
        }
      },
      playback: widget.hide ? Playback.PLAY_REVERSE : Playback.PLAY_FORWARD,
      duration: Duration(milliseconds: 300),
      tween: Tween(begin: 0.0, end: 1.0),
      builder: (context, value) => buildContainer(context, value),
    );
  }

  Widget buildContainer(BuildContext context, double value) {
    return ClipRect(
        child: Align(
      heightFactor: value,
      widthFactor: 1.0,
      //no overflow errors
      child: widget.child,
    ));
  }
}
