import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_miruna/settingservice.dart';

class SettingsPage extends StatefulWidget {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  var data;
  bool autoValidate = true;
  bool readOnly = false;
  bool showSegmentedControl = true;
  Timer t;
  Map initialState;
  bool isValid = true;
  int resetSeq = 0;
  bool _showSMSPreview = false;

  // TODO update this with setState and disable the submit
  bool formValid = false;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  // final GlobalKey<FormFieldState> _specifyTextFieldKey =
  //     GlobalKey<FormFieldState>();

  @override
  void initState() {
    super.initState();
    initialState = SettingsService().toMap();
  }

  @override
  void dispose() {
    super.dispose();
  }

  FormBuilderState get currentState {
    return widget._fbKey.currentState;
  }

  Map get currentValue {
    return widget._fbKey.currentState != null
        ? widget._fbKey.currentState.value
        : initialState;
  }

  @override
  Widget build(BuildContext context) {
    // Use the Todo to create the UI.
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Setări'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            DefaultTextStyle(
              style: Theme.of(context).textTheme.display1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      child: Text(
                        'Setări',
                        softWrap: true,
                        textAlign: TextAlign.center,
                      ),
                      padding: EdgeInsets.only(bottom: 16, top: 16),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: FormBuilder(
                  onChanged: (values) {
                    if (currentState.saveAndValidate()) {
                      debugPrint("VALID");
                      setState(() {
                        isValid = true;
                      });
                    } else {
                      debugPrint("INVALID");
                      setState(() {
                        isValid = false;
                      });
                    }
                    debugPrint("form changed ${values.toString()}");

                    // recompute(formState);
                  },
                  // context,
                  key: widget._fbKey,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  initialValue: initialState,
                  child: Column(
                    children: <Widget>[
                      FormBuilderSwitch(
                        attribute: 'sendNotifications',
                        initialValue: initialState['sendNotifications'],
                        label: Text(
                            'Trimite SMS \n(trebuie activat pe cel puţin 1 telefon)'),
                      ),
                      FormBuilderSwitch(
                        attribute: 'testMode',
                        initialValue: initialState['testMode'],
                        label: Text(
                            'Rulează în modul de testare\n(trimite SMS la nr. de testare în loc de nr. cilentului)'),
                      ),
                      Visibility(
                        visible: currentValue['testMode'] == false,
                        child: Container(
                            padding: EdgeInsets.all(16),
                            decoration: BoxDecoration(
                                border:
                                    Border.all(width: 1.0, color: Colors.red)),
                            child: Row(
                              children: <Widget>[
                                Icon(
                                  Icons.warning,
                                  color: Colors.red,
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Expanded(
                                  child: Text(
                                    'Atenţie! Se vor trimite notificări clienţilor. \nE pe bune!',
                                    textScaleFactor: 1.1,
                                  ),
                                ),
                              ],
                            )),
                      ),
                      FormBuilderTextField(
                        key: ValueKey("name-$resetSeq"),
                        attribute: "testPhoneNumber",
                        decoration: InputDecoration(
                          labelText: "Nr. de telefon pentru test",
                        ),
                        // onChanged: (val) => _onChanged("name", val,
                        //     values: currentValue),
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        initialValue: initialState['testPhoneNumber'],
                        validators: [
                          FormBuilderValidators.minLength(10,
                              errorText:
                                  "Nr. de telefon trebuie să aibă cel puţin 10 cifre"),
                          FormBuilderValidators.required(
                              errorText:
                                  "Trebuie să completaţi nr. de telefon"),
                        ],
                        keyboardType: TextInputType.phone,
                      ),
                      FormBuilderTextField(
                        key: ValueKey("phone-$resetSeq"),
                        attribute: "notificationTemplate",
                        decoration: InputDecoration(
                          labelText: "Şablon pentru SMS",
                        ),
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        initialValue: initialState['notificationTemplate'],
                        validators: [
                          FormBuilderValidators.required(
                              errorText: "Trebuie să completaţi şablonul"),
                        ],
                        keyboardType: TextInputType.text,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16.0),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: MaterialButton(
                                disabledColor: Colors.grey,
                                color: Theme.of(context).accentColor,
                                child: Text(
                                  "Salvează",
                                  style: TextStyle(color: Colors.white),
                                ),
                                onPressed: !isValid ? null : saveSettings,
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Expanded(
                              child: MaterialButton(
                                color: Theme.of(context).accentColor,
                                child: Text(
                                  "Reset",
                                  style: TextStyle(color: Colors.white),
                                ),
                                onPressed: () {
                                  setState(() {
                                    resetSeq++;
                                    currentState.reset();
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  saveSettings() async {
    if (currentState.saveAndValidate()) {
      SettingsService().fromMap(currentValue);
      String msg;
      await SettingsService().saveSettings().then((_) {
        msg = 'Setările au fost memorate';
      }).catchError((e) {
        msg = 'Nu s-au putut memora setările';
      });
      _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(msg)));
    } else {
      print(currentValue);
      print("validation failed");
    }
  }
}
