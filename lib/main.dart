import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_miruna/dataservice.dart';
import 'package:flutter_miruna/pages/home_page.dart';
import 'package:flutter_miruna/smsservice.dart';
import 'package:flutter_miruna/store/models.dart';
import 'package:flutter_miruna/store/ui.dart';
import 'package:intl/date_symbol_data_local.dart' as df;
import 'package:intl/intl.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';

void main() async {
  debugPrint(" ************** APP START **************");
  Intl.defaultLocale = 'ro_RO';
  await df.initializeDateFormatting('ro_RO');
  WidgetsFlutterBinding.ensureInitialized();

  PackageInfo.fromPlatform()
      .then((PackageInfo info) => UI().packageInfo = info);

  await AndroidAlarmManager.initialize();
  final int helloAlarmID = 0;
  await AndroidAlarmManager.cancel(helloAlarmID);
  bool alarmOK = await AndroidAlarmManager.periodic(
    const Duration(minutes: 5),
    helloAlarmID,
    SMSService.sendDueNotifications,
    exact: true,
    wakeup: true,
  );
  debugPrint('Alarm status: ${alarmOK}');
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'AutoReminders',
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.teal,
        ),
        home: MultiProvider(
          providers: [
            ChangeNotifierProvider(builder: (_) => UI()),
            ChangeNotifierProvider(builder: (_) => Models()),
          ],
          child: MyHomePage(title: 'AutoReminders'),
        ));
  }
}
