import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

final db = Firestore.instance;

Future<void> firestoreSignIn() async {
  await FirebaseAuth.instance.signInAnonymously();
}
