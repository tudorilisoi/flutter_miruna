import 'package:flutter/foundation.dart';
import 'package:flutter_miruna/store/changenotifier.dart';
import 'package:flutter_miruna/store/models/BaseModel.dart';

class Models extends LoggedChangeNotifier {
  factory Models() => _instance;

  Models._internal() {
    // this holds both phone contacts and DB clients
    // contacts and clients are just derived cached values from _people
    _people = [];

    _contacts = [];
    _clients = [];
  }

  // loading state
  bool _loading = false;

  bool get loading => _loading;

  set loading(bool isLoading) {
    _loading = isLoading;
    notifyListeners();
  }

  String _error;

  String get error => _error;

  set error(String error) {
    _error = error;
    notifyListeners();
  }

  List<BaseModel> _people = [];

  void recomputeLists({bool notify = true}) {
    _clients = _people.where((i) {
      return i.ref != null;
    }).toList();
    _contacts = _people.where((i) {
      return i.ref == null && i.contactIdentifier != null;
    }).toList();
    debugPrint('\n\n Recompute: ${_clients.length} CLIENTS');

    _clients.sort((a, b) => a.name.compareTo(b.name));
    _contacts.sort((a, b) => a.name.compareTo(b.name));
    if (notify) {
      notifyListeners();
    }
    debugPrint('\n\n Recompute: ${_contacts.length} CONTACTS');
    debugPrint('recomputeLists() DONE');
  }

  void empty() {
    _people = [];
    recomputeLists();
  }

  void removePerson(BaseModel model) {
    final idx = _people.indexWhere((m) => m.id == model.id);
    if (idx != -1) {
      _people.removeAt(idx);
    } else {
      debugPrint('***ERR cannot remove ${model.id}');
    }
    recomputeLists();
  }

  void _addPeople(List<BaseModel> list) {
    for (BaseModel m in list) {
      final idx = _people.indexWhere((i) => i.id == m.id);
      BaseModel existing = idx == -1 ? null : _people[idx];

      if (existing == null) {
        _people.add(m);
      } else {
        //NOTE whenever a FB document comes in, replace it
        if (m.ref != null) {
          debugPrint('*** EXISTING CONTACT UPDATE ${m.name}');
          _people[idx] = m;
        }
      }
    }
    recomputeLists();
  }

  List<BaseModel> _contacts;

  List<BaseModel> get contacts => _contacts;

  set contacts(List<BaseModel> newContacts) {
    _contacts.forEach((m) => removePerson(m));
    recomputeLists(notify: false);
    _addPeople(newContacts);
  }

  List<BaseModel> _clients;

  List<BaseModel> get clients => _clients;

  set clients(List<BaseModel> newClients) {
    _clients.forEach((m) => removePerson(m));
    recomputeLists(notify: false);
    _addPeople(newClients);
  }

  static final Models _instance = Models._internal();
}
