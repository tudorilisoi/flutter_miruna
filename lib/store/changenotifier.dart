import 'package:flutter/material.dart';

class LoggedChangeNotifier extends ChangeNotifier {
  int _listeners = 0;
  @override
  void removeListener(VoidCallback listener) {
    super.removeListener(listener);
    _listeners--;
    debugPrint('$runtimeType listeners: $_listeners');
  }

  @override
  void notifyListeners() {
    debugPrint('$runtimeType NOTIFY listeners: $_listeners');
    super.notifyListeners();
  }

  @override
  void addListener(VoidCallback listener) {
    super.addListener(listener);
    _listeners++;
    debugPrint('$runtimeType listeners: $_listeners');
  }
}

// ignore: non_constant_identifier_names
Builder React(fn) {
  return Builder(
    builder: (context) => fn(context),
  );
}

//Yes, I'm a React dev
class UseState extends LoggedChangeNotifier {
  UseState(this.name, this._state,
      {this.distinctValues = false, this.onChange});
  final String name;
  bool distinctValues;
  Function onChange;
  var _state;

  dynamic get state => _state;

  setState(dynamic state, {notify: true}) {
    if (this.distinctValues && state == _state) {
      return;
    }
    debugPrint("state $name changes to ${state.toString()}");
    _state = state;
    if (this.onChange != null) {
      this.onChange(this);
    }
    if (notify) {
      notifyListeners();
    }
  }

  set state(dynamic state) {
    setState(state, notify: true);
  }

  bool _mounted = true;
  bool get mounted => _mounted;

  @override
  // ignore: must_call_super
  void dispose() {
    //never dispose since we want the state to live thoughout the app lifecycle
    // super.dispose();
    _mounted = false;
  }

  Map<String, dynamic> clone() {
    return Map.from(state);
  }

  static subscribe(ChangeNotifier stateInstance, Function callback) {
    stateInstance.addListener(() {
      debugPrint("UseState ${stateInstance.runtimeType} got update");
      callback();
    });
  }
}

class _UseStateWrapper extends InheritedWidget {
  const _UseStateWrapper({
    Key key,
    this.child,
    // this.updateSeq
  }) : super(key: key);
  // final int updateSeq;
  final Widget child;

  Widget build(BuildContext context) {
    return child;
    // return Text("${key.toString()}");
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }
}

/// Wrapper for stateful functionality to provide onInit calls in stateles widget
class WithState extends StatefulWidget {
  final Map<String, dynamic> stateContainers;
  final Widget child;
  final State statefulWidget;
  const WithState(
      {Key key,
      @required this.stateContainers,
      @required this.child,
      this.statefulWidget})
      : super(key: key);
  @override
  _WithStateState createState() => _WithStateState();
}

class _WithStateState extends State<WithState> {
  int updateSeq;
  List<Function> _listeners = [];
  @override
  void initState() {
    updateSeq = 0;
    // if(widget.onInit != null) {
    //   widget.onInit();
    // }

    widget.stateContainers.keys.forEach((k) {
      ChangeNotifier c = widget.stateContainers[k];
      final l = () {
        // trigger repaint
        if (mounted) {
          debugPrint("got update $k");
          setState(() {
            updateSeq++;
          });
          if (widget.statefulWidget != null) {
            widget.statefulWidget.setState(() {});
          }
        }
      };
      c.addListener(l);
      _listeners.add(l);
    });
    super.initState();
  }

  @override
  void dispose() {
    int i = 0;
    widget.stateContainers.keys.forEach((k) {
      ChangeNotifier c = widget.stateContainers[k];
      c.removeListener(_listeners[i++]);
    });
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    debugPrint("withState is building...$updateSeq");
    return _UseStateWrapper(
        child: widget.child,
        // using the key to force a refresh
        key: ValueKey(updateSeq));
  }
}
